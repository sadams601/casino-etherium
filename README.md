### A simple ethereum casino

Running locally:

```
npm i -g webpack http-server
npm install
webpack
http-server dist/
```

Then open browser and goto http://127.0.0.1:8080/

Tutorial used:

* https://medium.com/@merunasgrincalaitis/the-ultimate-end-to-end-tutorial-to-create-and-deploy-a-fully-descentralized-dapp-in-ethereum-18f0cf6d7e0e

TODO: Hope to break off this dapp into it's own currency via this tutorial.

* https://hackernoon.com/heres-how-i-built-a-private-blockchain-network-and-you-can-too-62ca7db556c0
